public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Light light = new Light();
		LightOffCommand lightOffCommand = new LightOffCommand(light);
		RemoteControl remoteControl = new RemoteControl(lightOffCommand);
		remoteControl.pressButton();
		
		LightOnCommand lightOnCommand = new LightOnCommand(light);
		remoteControl.setCommand(lightOnCommand);
		remoteControl.pressButton();
	}

}
