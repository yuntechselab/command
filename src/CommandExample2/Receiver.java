package CommandExample2;

public abstract class Receiver {
	public abstract void turnOff();
	public abstract void turnOn();

}
