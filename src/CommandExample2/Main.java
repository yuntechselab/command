package CommandExample2;

public class Main {
	public static void main(String[] args) {
		Invoker_RemoteControl ir = new Invoker_RemoteControl();
		Receiver tv = new ReceiverTv();
		Command turnOffCommand = new TurnOffCommand();
		
		turnOffCommand.setReceiver(tv);
		ir.setCommand(turnOffCommand);
		ir.pressButton();
		
		Command turnOnCommand = new TurnOnCommand();
		turnOnCommand.setReceiver(tv);
		ir.setCommand(turnOnCommand);
		ir.pressButton();
		
		Receiver light = new Light();
		turnOnCommand.setReceiver(light);
		ir.setCommand(turnOnCommand);
		ir.pressButton();
		
		Receiver light_off = new Light();
		turnOffCommand.setReceiver(light_off);
		ir.setCommand(turnOffCommand);
		ir.pressButton();
	}
}
